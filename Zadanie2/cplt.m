function cplt(filename, filename2)
    arr=csvread(filename);
    x=arr(:,1);
    y=arr(:,2);
    plot(x,y);
    hold on;

    arr=csvread(filename2);
    x=arr(:,1);
    y=arr(:,2);
    plot(x,y);

    axis([0 2000 0 20]);
    title(strcat(filename, "\tvs\t", filename2));
    legend(filename, filename2);
    xlabel("SIZE");
    ylabel("time [s]");
endfunction