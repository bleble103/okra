function plt(filename)
    arr=csvread(filename);
    x=arr(:,1);
    y=arr(:,2);
    plot(x,y);
    axis([0 2000 0 20]);
    title(filename);
    xlabel("SIZE");
    ylabel("time [s]");
endfunction

