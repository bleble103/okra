#include <stdio.h>
#include <stdlib.h>
 

#include <sys/time.h>
#include <time.h>

#define A(m,n) (A[m*SIZE+n])

int SIZE;

static double gtod_ref_time_sec = 0.0;

/* Adapted from the bl2_clock() routine in the BLIS library */

double dclock()
{
  double         the_time, norm_sec;
  struct timeval tv;
  gettimeofday( &tv, NULL );
  if ( gtod_ref_time_sec == 0.0 )
    gtod_ref_time_sec = ( double ) tv.tv_sec;
  norm_sec = ( double ) tv.tv_sec - gtod_ref_time_sec;
  the_time = norm_sec + tv.tv_usec * 1.0e-6;
  return the_time;
}

void computeStep(double *ri, double *rk, int k){
    register int j;
    register int localsize = SIZE;
    for(j=k+1; j<localsize; j++){
        ri[j]=ri[j]-rk[j]*(ri[k]/rk[k]);
    }
}

#define R(i) (r[i*SIZE])
void compute4Rows(double *r, double *rk, int k){
    register int j;
    double rkk = rk[k];
    register double mult0 = (r[k]/rkk);
    register double mult1 = (r[SIZE+k]/rkk);
    register double mult2 = (r[2*SIZE+k]/rkk);
    register double mult3 = (r[3*SIZE+k]/rkk);
    register int localsize = SIZE;


    double *rj = &r[k+1];
    double *r1j = &r[localsize+k+1];
    double *r2j = &r[2*localsize+k+1];
    double *r3j = &r[3*localsize+k+1];
    for(j=k+1; j<localsize; j++){

        register double rkj = rk[j];

        *rj=*rj-rkj*mult0;
        *r1j=*r1j-rkj*mult1;
        *r2j=*r2j-rkj*mult2;
        *r3j=*r3j-rkj*mult3;

        rj ++;
        r1j ++;
        r2j ++;
        r3j ++;
    }
}

int ge(double* A)
{
  register int i,j,k;
  register int localsize = SIZE;
  for (k = 0; k < localsize; k++) { 
    for (i = k+1; i < localsize; i+=4) {

      if(i+4 >= localsize){
        for(j=i; j<localsize; j++){
            computeStep(&A(j,0), &A(k,0), k);
        }
        break;
      }else{

        compute4Rows(&A(i,0), &A(k,0), k); 
      }
      
    }
  }
  return 0;
}


int main( int argc, const char* argv[] )
{
  int i,j,k,iret;
  //double matrix[SIZE][SIZE]; // TODO - make dynamic allocation
  SIZE = 1000;
  for(SIZE=100; SIZE <= 2000; SIZE += 100){
    double *A = (double*)malloc(SIZE*SIZE*sizeof(double));
    double dtime;
    srand(1);
    for (i = 0; i < SIZE; i++) { 
      for (j = 0; j < SIZE; j++) { 
        A(i,j)=rand();
      }
    }
    //printf("call GE");
    dtime = dclock();
    iret=ge(A);
    dtime = dclock()-dtime;
    printf( "%d,%le\n",SIZE, dtime);

    double check=0.0;
    for (i = 0; i < SIZE; i++) {
      for (j = 0; j < SIZE; j++) {
        check = check + A(i,j);
      }
    }
    //printf( "Check: %le \n", check);
    fflush( stdout );
    free(A);
  }


  return iret;
}

