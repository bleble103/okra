#include <stdio.h>
#include <stdlib.h>
 

#include <sys/time.h>
#include <time.h>

#define A(m,n) (A[m*SIZE+n])

int SIZE;

static double gtod_ref_time_sec = 0.0;

/* Adapted from the bl2_clock() routine in the BLIS library */

double dclock()
{
  double         the_time, norm_sec;
  struct timeval tv;
  gettimeofday( &tv, NULL );
  if ( gtod_ref_time_sec == 0.0 )
    gtod_ref_time_sec = ( double ) tv.tv_sec;
  norm_sec = ( double ) tv.tv_sec - gtod_ref_time_sec;
  the_time = norm_sec + tv.tv_usec * 1.0e-6;
  return the_time;
}

void computeStep(double *ri, double *rk, int k){
    register int j;
    for(j=k+1; j<SIZE; j++){
        ri[j]=ri[j]-rk[j]*(ri[k]/rk[k]);
    }
}

#define R(i) (r[i*SIZE])
void compute4Rows(double *r, double *rk, int k){
    register int j;
    double rkk = rk[k];
    register double mult0 = (r[k]/rkk);
    register double mult1 = (r[SIZE+k]/rkk);
    register double mult2 = (r[2*SIZE+k]/rkk);
    register double mult3 = (r[3*SIZE+k]/rkk);

    for(j=k+1; j<SIZE; j++){
        double *rj = &r[j];
        double *r1j = &r[SIZE+j];
        double *r2j = &r[2*SIZE+j];
        double *r3j = &r[3*SIZE+j];

        register double rkj = rk[j];

        *rj=*rj-rkj*mult0;
        *r1j=*r1j-rkj*mult1;
        *r2j=*r2j-rkj*mult2;
        *r3j=*r3j-rkj*mult3;
    }
}

int ge(double* A)
{
  register int i,j,k;
  for (k = 0; k < SIZE; k++) { 
    for (i = k+1; i < SIZE; i+=4) {

      if(i+4 >= SIZE){
        for(j=i; j<SIZE; j++){
            computeStep(&A(j,0), &A(k,0), k);
        }
        break;
      }else{

        compute4Rows(&A(i,0), &A(k,0), k); 
      }
      
    }
  }
  return 0;
}


int main( int argc, const char* argv[] )
{
  int i,j,k,iret;
  //double matrix[SIZE][SIZE]; // TODO - make dynamic allocation
  SIZE = 1000;
  for(SIZE=100; SIZE <= 2000; SIZE += 100){
    double *A = (double*)malloc(SIZE*SIZE*sizeof(double));
    double dtime;
    srand(1);
    for (i = 0; i < SIZE; i++) { 
      for (j = 0; j < SIZE; j++) { 
        A(i,j)=rand();
      }
    }
    //printf("call GE");
    dtime = dclock();
    iret=ge(A);
    dtime = dclock()-dtime;
    printf( "%d,%le\n",SIZE, dtime);

    double check=0.0;
    for (i = 0; i < SIZE; i++) {
      for (j = 0; j < SIZE; j++) {
        check = check + A(i,j);
      }
    }
    //printf( "Check: %le \n", check);
    fflush( stdout );
    free(A);
  }


  return iret;
}

