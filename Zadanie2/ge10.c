#include <stdio.h>
#include <stdlib.h>
 

#include <sys/time.h>
#include <time.h>
#include <mmintrin.h>
#include <xmmintrin.h>  // SSE
#include <pmmintrin.h>  // SSE2
#include <emmintrin.h>  // SSE3

typedef union
{
  __m128d v;
  double d[2];
} v2df_t;

#define A(m,n) (A[m*SIZE+n])

int SIZE;

static double gtod_ref_time_sec = 0.0;

/* Adapted from the bl2_clock() routine in the BLIS library */

double dclock()
{
  double         the_time, norm_sec;
  struct timeval tv;
  gettimeofday( &tv, NULL );
  if ( gtod_ref_time_sec == 0.0 )
    gtod_ref_time_sec = ( double ) tv.tv_sec;
  norm_sec = ( double ) tv.tv_sec - gtod_ref_time_sec;
  the_time = norm_sec + tv.tv_usec * 1.0e-6;
  return the_time;
}

void computeStep(double *ri, double *rk, int k){
    register int j;
    register int localsize = SIZE;
    for(j=k+1; j<localsize; j++){
        ri[j]=ri[j]-rk[j]*(ri[k]/rk[k]);
    }
}

#define R(i) (r[i*SIZE])
void compute4Rows(double *r, double *rk, int k){
    v2df_t a0_0_a0_1, a1_0_a1_1, a2_0_a2_1, a3_0_a3_1, vreg_rkj, vreg_mult0, vreg_mult1, vreg_mult2, vreg_mult3;

    register int j;
    double rkk = rk[k];

    double mult0 = -(r[k]/rkk);
    double mult1 = -(r[SIZE+k]/rkk);
    double mult2 = -(r[2*SIZE+k]/rkk);
    double mult3 = -(r[3*SIZE+k]/rkk);

    vreg_mult0.v = _mm_loaddup_pd((double *) &mult0);
    vreg_mult1.v = _mm_loaddup_pd((double*) &mult1);
    vreg_mult2.v = _mm_loaddup_pd((double*) &mult2);
    vreg_mult3.v = _mm_loaddup_pd((double*) &mult3);

    register int localsize = SIZE;

    if((k+1)%2 != 0){
        k++;
    }

    double *rj = &r[k+1];
    double *r1j = &r[localsize+k+1];
    double *r2j = &r[2*localsize+k+1];
    double *r3j = &r[3*localsize+k+1];
    

    for(j=k+1; j<localsize; j++){
        if(j+1 >= localsize){
            break;
        }
        a0_0_a0_1.v = _mm_load_pd(rj);
        a1_0_a1_1.v = _mm_load_pd(r1j);
        a2_0_a2_1.v = _mm_load_pd(r2j);
        a2_0_a2_1.v = _mm_load_pd(r3j);

        register double rkj = rk[j];

        vreg_rkj.v = _mm_loaddup_pd( (double*)( &(rk[j])) );

        a0_0_a0_1.v += vreg_rkj.v*vreg_mult0.v;
        a1_0_a1_1.v += vreg_rkj.v*vreg_mult1.v;
        a2_0_a2_1.v += vreg_rkj.v*vreg_mult2.v;
        a3_0_a3_1.v += vreg_rkj.v*vreg_mult3.v;

        rj[0] = a0_0_a0_1.d[0]; rj[1] = a0_0_a0_1.d[1];
        r1j[0] = a1_0_a1_1.d[0]; r1j[1] = a1_0_a1_1.d[1];
        r2j[0] = a2_0_a2_1.d[0]; r2j[1] = a2_0_a2_1.d[1];
        r3j[0] = a3_0_a3_1.d[0]; r3j[1] = a3_0_a3_1.d[1];


        rj +=2;
        r1j +=2;
        r2j +=2;
        r3j +=2;
    }
}

int ge(double* A)
{
  register int i,j,k;
  register int localsize = SIZE;
  for (k = 0; k < localsize; k++) { 
    for (i = k+1; i < localsize; i+=4) {

      if(i+4 >= localsize){
        for(j=i; j<localsize; j++){
            computeStep(&A(j,0), &A(k,0), k);
        }
        break;
      }else{

        compute4Rows(&A(i,0), &A(k,0), k); 
      }
      
    }
  }
  return 0;
}


int main( int argc, const char* argv[] )
{
  int i,j,k,iret;
  //double matrix[SIZE][SIZE]; // TODO - make dynamic allocation
  SIZE = 1000;
  for(SIZE=100; SIZE <= 2000; SIZE += 100){
    double *A = (double*)malloc(SIZE*SIZE*sizeof(double));
    double dtime;
    srand(1);
    for (i = 0; i < SIZE; i++) { 
      for (j = 0; j < SIZE; j++) { 
        A(i,j)=rand();
      }
    }
    //printf("call GE");
    dtime = dclock();
    iret=ge(A);
    dtime = dclock()-dtime;
    printf( "%d,%le\n",SIZE, dtime);

    double check=0.0;
    for (i = 0; i < SIZE; i++) {
      for (j = 0; j < SIZE; j++) {
        check = check + A(i,j);
      }
    }
    //printf( "Check: %le \n", check);
    fflush( stdout );
    free(A);
  }


  return iret;
}

